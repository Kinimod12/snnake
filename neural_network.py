import time
import argparse
from random import randint
from snake_game import SnakeNoRender
from config import Config

import numpy as np
import pygame
import sys

import tflearn
import math
from tflearn.layers.core import input_data, fully_connected
from tflearn.layers.estimator import regression
from statistics import mean
from collections import Counter

class NeuralNetwork():
    def __init__(self, args, test_games=100, goal_steps=3000, lr = 1e-2, filename = 'snake_nn.tflearn'):
        self.args = args
        self.test_games = test_games
        self.goal_steps = goal_steps 
        self.lr = lr
        self.filename = filename
        self.vectors_and_keys = {
        # [0, -1]UP 
        # [1, 0]RIGHT 
        # [0, 1]DOWN
        # [-1, 0]LEFT
            0: [0, -1],
            1: [1, 0],
            2: [0, 1],
            3: [-1, 0] 
        }
        self.cfg = Config.get_config()

    def initial_population(self, initial_games):
        training_data = []

        for game_number in range(initial_games):
            game = SnakeNoRender()
            print(f'Playing game: {game_number}/{initial_games}', end='\r')
            _, prev_game_score, snake, _, food = game.generate_observations()
            prev_observation = self.generate_observation(snake, food)
            prev_food_distance = self.get_food_distance(snake, food)
            for _ in range(self.goal_steps):
                if np.isnan(prev_observation).any():
                    prev_observation = np.nan_to_num(prev_observation)
                action, game_action = self.generate_action(snake)
                done, game_score, snake, _, food  = game.tick(new_direction=game_action)
                if done:
                    training_data.append([self.add_action_to_observation(prev_observation, action), -1])
                    break
                else:
                    food_distance = self.get_food_distance(snake, food)
                    if prev_game_score < game_score:
                        training_data.append([self.add_action_to_observation(prev_observation, action), 1])
                    elif food_distance <= prev_food_distance:
                        training_data.append([self.add_action_to_observation(prev_observation, action), 1])
                    else: 
                        training_data.append([self.add_action_to_observation(prev_observation, action), 0])
                    prev_game_score = game_score
                    prev_observation = self.generate_observation(snake, food)
                    
                    prev_food_distance = food_distance

        print(len(training_data))
        np.save(f'training_data_{initial_games}.npy', training_data)
        return training_data       

    def generate_observation(self, snake, food, console_log=False):
        snake_direction_vector = self.get_snake_direction_vector(snake)

        food_direction_vector = self.get_food_direction_vector(snake, food)
        tail_direction_vector = self.get_tail_direction_vector(snake)

        barrier_left = self.is_direction_blocked(snake, snake_direction_vector, check_direction='left')
        barrier_front = self.is_direction_blocked(snake, snake_direction_vector, check_direction='front')
        barrier_right = self.is_direction_blocked(snake, snake_direction_vector, check_direction='right')
        
        snake_diretion_to_food_angle = self.get_angle(snake_direction_vector, food_direction_vector)
        tail_to_food_angle = self.get_angle(tail_direction_vector, food_direction_vector)

        if console_log:
            print(f'Snake head: {np.array(snake[0])}\n')
            print(f'Barrier on the left: {barrier_left}')
            print(f'Barrier in the front: {barrier_front}')
            print(f'Barrier on the right: {barrier_right}\n')
            print(f'Angle between snake directon and food direction: {snake_diretion_to_food_angle}')
            print(f'Angle between tail directon and food direction: {tail_to_food_angle}')

        return np.array([int(barrier_left), 
            int(barrier_front), 
            int(barrier_right), 
            float(snake_diretion_to_food_angle), 
            float(tail_to_food_angle)])
    
    def add_action_to_observation(self, observation, action):
        return np.append([action], observation)
    
    def get_snake_direction_vector(self, snake):
        return np.array(snake[0]) - np.array(snake[1])
    
    def get_food_direction_vector(self, snake, food, console_log=False):
        food_direction_vector = np.array(food) - np.array(snake[0])
        if console_log:
            print(f'Food direction vector: {food_direction_vector}')
        return food_direction_vector
    
    def get_tail_direction_vector(self, snake, console_log=False):
        food_direction_vector = np.array(snake[-1]) - np.array(snake[0])
        if console_log:
            print(f'Tail direction vector: {food_direction_vector}')
        return food_direction_vector

    def normalize_vector(self, vector):
        return vector / np.linalg.norm(vector)

    def get_food_distance(self, snake, food, console_log=False):
        distance = np.linalg.norm(self.get_food_direction_vector(snake, food))
        if console_log:
            print(f'Food distance: {distance}')
        return distance

    def generate_action(self, snake):
        action = randint(-1, 1)
        return action, self.get_game_action(snake, action)
    
    def get_game_action(self, snake, action, console_log=False):
        snake_direction = self.get_snake_direction_vector(snake)
        if console_log:
            print(f'Action in get_game_action: {action}')
        new_direction = snake_direction
        if action == -1:
            new_direction = self.turn_vector_to_the_left(snake_direction)
        elif action == 1:
            new_direction = self.turn_vector_to_the_right(snake_direction)
        
        for game_act, direction in self.vectors_and_keys.items():  
            if direction == new_direction.tolist():
                game_action = game_act
        return game_action

    def turn_vector_to_the_left(self, vector):  
        # [1, 0]RIGHT -> to left [0, -1]UP
        # [0, -1]UP -> to left [-1, 0]LEFT
        # [-1, 0]LEFT -> to left [0, 1]DOWN
        # [0, 1]DOWN -> to left [1, 0]RIGHT
        return np.array([vector[1], -vector[0]])

    def turn_vector_to_the_right(self, vector):
        # [1, 0]RIGHT -> to right [0, 1]DOWN
        # [0, 1]DOWN -> to right [-1, 0]LEFT 
        # [-1, 0]LEFT -> to right [0, -1]UP
        # [0, -1]UP -> to right [1, 0]RIGHT
        return np.array([-vector[1], vector[0]])
    
    def get_angle(self, a, b):
        a = self.normalize_vector(a)
        b = self.normalize_vector(b)
        angle = math.atan2(a[0] * b[1] - a[1] * b[0], a[0] * b[0] + a[1] * b[1]) / math.pi
        float(angle)
        return angle

    def is_direction_blocked(self, snake, snake_direction_vector, check_direction, console_log=False):
        if check_direction == 'left':
            check_direction_vector = self.turn_vector_to_the_left(snake_direction_vector)
        if check_direction == 'front':
            check_direction_vector = snake_direction_vector
        if check_direction == 'right':
            check_direction_vector = self.turn_vector_to_the_right(snake_direction_vector)
        
        point = np.array(snake[0]) + np.array(check_direction_vector)
        if console_log:
            print(f'{check_direction} direction vector: {np.array(check_direction_vector)}\n')
            print(f'Next point in that direction: {point}\n')
            print(f'Is {point.tolist()} in {snake[1:]}: {point.tolist() in snake[1:]}')
        return point.tolist() in snake[1:] or point[0] == 0 or point[1] == 0 or point[0] == 31 or point[1] == 31

    def model(self):
        network = input_data(shape=[None, 6, 1], name='input')
        network = fully_connected(network, 16, activation='relu')
        network = fully_connected(network, 16, activation='relu')
        network = fully_connected(network, 1, activation='sigmoid')
        network = regression(network, optimizer='adam', learning_rate=self.lr, loss='mean_square', name='target')
        model = tflearn.DNN(network, tensorboard_dir='log')
        return model
    
    def train_model(self, training_data, model):
        X = np.array([i[0] for i in training_data]).reshape(-1, 6, 1)
        y = np.array([i[1] for i in training_data]).reshape(-1,1)
        print(X[np.argwhere(np.isnan(X))])
        model.fit(X,y, n_epoch = 3, shuffle = True, run_id = self.filename)
        model.save(self.filename)
        return model
    
    def train(self):
        training_data = []
        if self.args.generate:
            training_data = self.initial_population(self.args.initial_games)
        if self.args.train_model:
            nn_model = self.model()
            if not training_data:
                training_data = np.load(f'training_data_{self.args.initial_games}.npy', allow_pickle=True)
            nn_model = self.train_model(training_data, nn_model)
    
    def visualise_game(self, model):
        pygame.init()
        screen_width = self.cfg['SCREEN_WIDTH']
        screen_height = self.cfg['SCREEN_HEIGHT']
        margin = self.cfg['MARGIN']
        screen = pygame.display.set_mode([screen_width, screen_height])

        game = SnakeNoRender()
        _, _, snake, board_state, food = game.generate_observations()
        prev_observation = self.generate_observation(snake, food)

        for _ in range(self.goal_steps):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
            screen.fill((181, 215, 187))
            for y, row in enumerate(board_state):
                for x, element in enumerate(row):
                    color = self.cfg['colors'][element]
                    pygame.draw.rect(screen, color, ( 10+x*(20+margin) , 10+y*(20+margin),20,20))
            pygame.display.flip()
            time.sleep(0.05)
            predictions = []
            for action in range(-1, 2):
               predictions.append(model.predict(self.add_action_to_observation(prev_observation, action).reshape(-1, 6, 1)))

            action = np.argmax(np.array(predictions))
            print(predictions)
            game_action = self.get_game_action(snake, action-1)
            done, _, snake, board_state, food  = game.tick(game_action)
            if done:
                break
            else:
                prev_observation = self.generate_observation(snake, food, console_log=True)
        while True:
            screen.fill((181, 215, 187))
            for y, row in enumerate(board_state):
                for x, element in enumerate(row):
                    color = self.cfg['colors'][element]
                    pygame.draw.rect(screen, color, (10 + x*(20+margin),10 + y*(20+margin),20,20))
            pygame.display.flip()

        pygame.quit()

    def visualise(self):
        nn_model = self.model()
        nn_model.load(self.filename)
        self.visualise_game(nn_model)
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument("-g", "--generate", help="generate initial dataset", action="store_true")
    parser.add_argument("-ig", "--initial_games", type=int, default=100, help="number of initial games to be played")
    parser.add_argument("-tm", "--train_model", help="whether to train the model", action="store_true")
    parser.add_argument("-v", "--visualise", help="visualise game using trained model", action="store_true")
    args = parser.parse_args()
    neural_network = NeuralNetwork(args=args)
    if args.train_model:
        neural_network.train()
    if args.visualise:
        neural_network.visualise()