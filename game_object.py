class GameObject:
    def __init__(self, go_type):
        self.types_dict = {
            0: 'background',
            1: 'snake',
            2: 'food',
            3: 'wall'
        }
        self.type = go_type
    
    def get_type(self):
        return self.types_dict[self.type]
