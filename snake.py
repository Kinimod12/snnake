import copy
from random import randint

class Snake:
    def __init__(self):
        self.head_coordinates = {
            'x': 16, 
            'y': 16}
        coordinates = [(self.head_coordinates['x'] - i, self.head_coordinates['y']) for i in range(3)]
        self.segments = [
            {
                'x': coordinate[0], 
                'y': coordinate[1]     
            }
            for coordinate in coordinates
        ]
        self.direction = 1
    
    def to_list(self):
        return [[value for value in segment.values()] for segment in self.segments]
    
    def get_next_head_position(self):
        if self.direction == 0:
            return self.head_coordinates['x'], self.head_coordinates['y'] - 1
        if self.direction == 1:
            return self.head_coordinates['x'] + 1, self.head_coordinates['y']
        if self.direction == 2:
            return self.head_coordinates['x'], self.head_coordinates['y'] + 1
        if self.direction == 3:
            return self.head_coordinates['x'] - 1, self.head_coordinates['y']
        else:
            raise ValueError('Your snake is trying to escape to 3rd dimension!')

    def move(self, next_x, next_y):
        new_head = {'x': next_x, 'y': next_y}
        self.segments = copy.deepcopy(self.segments[:-1])
        self.segments.insert(0, new_head)
        self.head_coordinates = new_head
    
    def grow(self, next_x, next_y):
        new_head = {'x': next_x, 'y': next_y}
        self.segments.insert(0, new_head)
        self.head_coordinates = new_head
