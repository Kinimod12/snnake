class Config:
    @staticmethod
    def get_config():
        cfg = {
            'SCREEN_WIDTH': 690,
            'SCREEN_HEIGHT': 690,
            'SCREEN_TITLE': "SNNAKE",
            'BOARD_WIDTH': 32,
            'BOARD_HEIGHT': 32,
            'MARGIN': 1,
            'WIDTH': 20,
            'HEIGHT': 20,
            'colors': {
                0: (243, 246, 210), 
                1: (97, 171, 160), 
                2: (169, 27, 13), 
                3: (97, 79, 101),
                10: (43, 143, 145)
            }
        }
        return cfg
