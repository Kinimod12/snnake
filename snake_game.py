from random import randint
import time
import pygame

from board import Board
from snake import Snake

class SnakeNoRender:
    def __init__(self, console_log=False):
        self.board = Board()
        self.snake = Snake()
        self.update_board()
        self.game_over = False
        self.score = 0
        self.console_log = console_log
    
    def update_board(self):
        self.board.update_snake(self.snake.segments)
        self.board_state = self.board.get_state()

    def tick(self, new_direction=None):
        if self.is_direction_valid(new_direction):
            if self.console_log:
                print(f'Changing direction from {self.snake.direction} to {new_direction}')
            self.snake.direction = new_direction
        else:
            if self.console_log:
                print('No direction change')

        next_x, next_y = self.snake.get_next_head_position()
        next_object_type = self.board_state[next_y][next_x].type
        
        if [next_x, next_y] in self.snake.to_list()[:-1]:
            self.game_over = True
        if next_object_type == 0 or next_object_type == 1:
            self.snake.move(next_x, next_y)
        if next_object_type == 2:
            self.snake.grow(next_x, next_y)
            self.score += 1
            self.board.generate_food()
        if next_object_type == 3:
            self.game_over = True
        self.update_board()
        return self.generate_observations()
    
    def generate_observations(self):
        return self.game_over, self.score, self.snake.to_list(), self.board.get_state(as_integers=True), self.board.food_coordinates 
    
    def is_direction_valid(self, direction):
        if direction == self.snake.direction:
            return False
        elif direction == 0 and self.snake.direction != 2:
            return True
        elif direction == 1 and self.snake.direction != 3:
            return True
        elif direction == 2 and self.snake.direction != 0:
            return True
        elif direction == 3 and self.snake.direction != 1:
            return True
        else:
            return False
    
    def main(self):
        for _ in range(20):
            action = randint(0, 3)
            _, _, snake, _  = self.tick(action)
            print(snake)

if __name__ == '__main__':
    snake_game = SnakeNoRender()
    snake_game.main()