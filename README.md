Welcome to Snake Neural Network(SNNake for short)!

For quick start run:\
    1. pip3 install -r requirements.txt\
    2. python3 neural_network.py\
    3. python3 neural_network.py -v

For additional information on run modes run:
    - python3 neural_network.py -h

This project consists of 2 essential parts:\
    A. The engine for the Snake game.\
    B. Script which uses the Snake game and trains neural network to play it\
    (or at least predict moves that result in survival and getting closer to food)

A. The Snake Clone:\
    Snake clone uses following classes:

    SnakeNoRender
    Snake
    Board
    GameObject
    Config

B. The NN script:\
    The neural_network.py script uses following classes:

    SnakeNoRender
    Config

Pygame is used for visualisation purposes

