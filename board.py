from config import Config
from game_object import GameObject
from random import randint

class Board:
    def __init__(self):
        cfg = Config.get_config()
        self.board_state = [[GameObject(0) for _x in range(cfg['BOARD_WIDTH'])] for _y in range(cfg['BOARD_HEIGHT'])]
        self.snake_position = []
        self.food_coordinates = None
        self.generate_food()
        self.generate_walls()

    
    def get_state(self, as_integers=False):
        if as_integers:
            return[[element.type for element in row] for row in self.board_state]
        return self.board_state
    
    def generate_food(self):
        while True:
            x = randint(1, 30)
            y = randint(1, 30)
            if self.board_state[y][x].type != 1:
                self.food_coordinates = [x, y]
                break
        self.board_state[y][x].type = 2
    
    def generate_walls(self):
        for i in range(32):
            self.board_state[0][i].type = 3
            self.board_state[31][i].type = 3
            self.board_state[i][0].type = 3
            self.board_state[i][31].type = 3
    
    def update_snake(self, segments_coordinates):
        if self.snake_position:
            self.erase_tail()
        self.board_state[segments_coordinates[0]['y']][segments_coordinates[0]['x']].type = 10
        for coordinates in segments_coordinates[1:]:
            x = coordinates['x']
            y = coordinates['y']
            self.board_state[y][x].type = 1
            self.snake_position.append({'x': x, 'y': y})

    def erase_tail(self):
        x = self.snake_position[-1]['x']
        y = self.snake_position[-1]['y']
        self.board_state[y][x].type = 0
        self.snake_position = []
    